#! /bin/env python3
import os, sys, json, smtplib, subprocess, time, \
    logging, config


user = config.SMTP_SERVER_USERNAME
pwd = config.SMTP_SERVER_PWD
to = config.RECIPIENTS_LIST
base_dir = config.BASE_DIR
filename = config.MASTERNODES_FILE


class Mailer:

    def build_email(self, coin, alias):
        subject = '{} Masternode reset'.format(coin)
        body = 'The following masternode has been reset.\n\n{} MN{}'.format(coin, alias)

        email_text = """\
        From: {}
        To: {}
        Subject: {}

        {}
        """.format(user, ", ".join(to), subject, body)
        return email_text


    def send_mail(self, email_text):
        try:
            server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
            server.login(user, pwd)
            server.sendmail(user, to, email_text)
            server.close()

            print('Email sent!')
        except Exception as e:
            print('Something went wrong...\n{}'.format(e))


class MNTools:

    def get_mn_status(self, coin_name, clitool, data_dir=None, mn_number=1):
        os.chdir(base_dir + coin_name)
        cmd = [
            "docker-compose", 
            "exec", 
            "mn{}".format(mn_number), 
            clitool
        ]
        if data_dir != "":
            cmd.extend(["--datadir={}".format(data_dir)])
        cmd.extend(["masternode", "status"])
        return subprocess.check_output(cmd)


    def get_mn_debug(self, coin_name, clitool, mn_number=1):
        os.chdir(base_dir + coin_name)
        cmd = [
            "docker-compose", 
            "exec", 
            "mn{}".format(mn_number), 
            clitool,
            "masternode",
            "debug"
        ]
        return subprocess.call(cmd)


    def mn_test(self, coin_name, clitool, data_dir, mn_number):
        status = json.loads(self.get_mn_status(coin_name, clitool, data_dir, mn_number))

        try:
            if "addr" in status.keys():
                addr = status["addr"]
            elif "pubkey" in status.keys():
                addr = status["pubkey"]
            elif "state" in status.keys():
                addr = status["state"]
            print("{}: alias MN{}: {}".format(coin_name, mn_number, addr))
            return True
        except:
            print("{}\n".format(status))
            return False


    def mn_reset(self, coin_name):
        os.chdir(base_dir + coin_name)
        subprocess.call(["docker-compose", "down"])
        time.sleep(60)
        subprocess.call(["docker-compose", "up", "-d"])


def run():
    mndata = json.load(open(filename, "r"))
    for m in mndata:
        rsl = MNTools().mn_test(m["coinName"], m["cliTool"], m['dataDir'], m["number"])
        if rsl == False:
            text = Mailer().build_email(m["coinName"], m["number"])
            Mailer().send_mail(text)
            # MNTools().mn_reset(m["coinName"])


if __name__ == "__main__":    
    # MNTools().mn_test(sys.argv[1], sys.argv[2], sys.argv[3])
    run()
