# MNMonitor

## About
MNMonitor is a simple python script used to monitor your masternodes. <br>
It will send an email in case it is unable to get the public key from <br>
its request.

## Requirements
You need to create a `config.py` file with the following paramaneters:
```
SMTP_SERVER_USERNAME = "" # SMTP server username
SMTP_SERVER_PWD = "" # SMTP server password
RECIPIENTS_LIST = [] # List of recipients
BASE_DIR = "" # Your base dir
MASTERNODES_FILE = "" # File to read masternodes from
```
and a `masternodesList.json` file with following schema:
```
[
  {
    "coinName": "",
    "cliTool": "",
    "number": ,
    "dataDir": ""
  },
  {...}
]
```
and place them in the current folder.


## How to Use

Then add the script has a cron job:
```
crontab -e
* */2 * * * python <path-to-script>/mastenodes_monitor.py # Will run the script every 2 hours.
```

